# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy



class GratkaItem(scrapy.Item):
    miejscowosc = scrapy.Field()
    dzielnica = scrapy.Field()
    ulica = scrapy.Field()
    powierzchnia = scrapy.Field()
    liczba_pokoi = scrapy.Field()
    pietro = scrapy.Field()
    liczba_pieter = scrapy.Field()
    typ_zabudowy = scrapy.Field()
    stan = scrapy.Field()
    oplaty_czynsz_media = scrapy.Field()
    cena = scrapy.Field()
    link = scrapy.Field()
