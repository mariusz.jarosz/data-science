# -*- coding: utf-8 -*-
import scrapy

from ..items import GratkaItem



class GratkaSpider(scrapy.Spider):
    name = 'gratka'


    lista_miast = ['wroclaw', 'bydgoszcz', 'lublin', 'gorzow-wielkopolski', 'lodz','krakow', 'warszawa', 'opole', 'rzeszow', 'bialystok', 'gdansk', 'katowice', 'kielce', 'olsztyn', 'poznan', 'szczecin']

    start_urls = []

    for i in lista_miast:
        start_urls.append(f'https://gratka.pl/nieruchomosci/mieszkania/{i}/sprzedaz?cena-calkowita:min=10000')

    # Jeżeli chcesz zmienić pozostałe parametry to lepiej odkomentować poniższą listę i zakomentować powyższą funkcję
    # (różnią się tylko miastem - można dodawać kolejne i zmienić pozostałe parametry)
    
#     start_urls = [
#    # 'https://gratka.pl/nieruchomosci/mieszkania/krakow/sprzedaz?cena-calkowita:min=10000&cena-calkowita:max=150000'
#         'https://gratka.pl/nieruchomosci/mieszkania/szczecin/sprzedaz?cena-calkowita:min=10000'        
#     ]


    custom_settings = {
    # lista pól i ich kolejność do eksportowania (domyślnie jest dowolna)
   'FEED_EXPORT_FIELDS': ['miejscowosc','dzielnica','ulica','powierzchnia','liczba_pokoi',
   'pietro', 'liczba_pieter','typ_zabudowy', 'stan', 'oplaty_czynsz_media','cena','link'],
  }
    def parse(self, response):
        
        items = GratkaItem()

        elementy = response.css(".teaser")
        
        for e in elementy:

            test = e.css('.teaser__params li::text').extract()
            
            powierzchnia = None
            liczba_pokoi = None
            pietro = None 
            liczba_pieter = None
            typ_zabudowy = None
            stan = None
            oplaty = None

            for i in test:
                if 'Powierzchnia' in i:
                    powierzchnia = i.split(' ')[-1] 
    
                if 'Piętro' in i:
                    pietro = i.split(':')[-1]

                if 'Liczba pokoi:' in i:
                    liczba_pokoi = i.split(':')[-1]
                
                if 'Liczba pięter' in i:
                    liczba_pieter = i.split(':')[-1]

                if 'Typ zabudowy' in i:
                    typ_zabudowy = i.split(':')[-1]

                if 'Stan' in i:
                    stan = i.split(':')[-1]

                if 'Opłaty (czynsz administracyjny, media)' in i:
                    oplaty = i.split(':')[-1]

            miejscowosc = e.css('.teaser__location::text').extract_first().strip().split(",")[0]
            dzielnica = str(e.css('.teaser__location::text').extract_first().strip().split(",")[1]).strip()
            ulica = e.css('.teaser__anchor::text').extract_first().strip().split(',')[-1].strip()

            cena = int(e.css('.teaser__price::text').extract_first().strip().replace(" ", ""))

            link = e.css('.teaser__anchor::attr(href)').extract_first()

            items['miejscowosc'] = miejscowosc  
            items['dzielnica'] = dzielnica   
            items['powierzchnia'] = powierzchnia 
            items['liczba_pokoi'] = liczba_pokoi
            items['pietro'] = pietro  
            items['liczba_pieter'] = liczba_pieter
            items['typ_zabudowy'] = typ_zabudowy
            items['stan'] = stan
            items['oplaty_czynsz_media'] = oplaty
            items['cena'] = cena 
            items['link'] = link
            items['ulica'] = ulica

            yield items
           
        nastepna_strona = response.css('.pagination__nextPage::attr(href)').extract_first()
        print(nastepna_strona)
        if nastepna_strona:
            yield scrapy.Request(
                response.urljoin(nastepna_strona),
                callback=self.parse
            )
        

#Zapisywanie scrapy crawl gratka -o gratka.csv