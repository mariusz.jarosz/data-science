# -*- coding: utf-8 -*-
import scrapy

from ..items import GoodreadsItem
from scrapy.loader import ItemLoader


class GoodreadsSpiderSpider(scrapy.Spider):
    name = 'goodreads'
    page_number = 2
    
    start_urls = [
        'https://www.goodreads.com/quotes']

    def parse(self, response):
        items = GoodreadsItem()

        all_elements = response.xpath("//div[@class='quoteDetails']")

        for e in all_elements:
            autor = e.xpath("normalize-space(.//span[@class='authorOrTitle']/text())").extract_first().replace(',','')
            cytat = e.xpath("normalize-space(.//div[@class='quoteText']/text())").extract_first().replace('\u201c','').replace('\u201d', '')
            polubienia = e.xpath(".//a[@class='smallText']/text()").extract_first().replace(' likes', '')
            tagi = e.xpath('.//div[@class="greyText smallText left"]/a/text()').extract()

        # cytat = response.css('.quoteText').css('::text').extract()

            items['autor'] = autor
            items['cytat'] = cytat 
            items['polubienia'] = polubienia
            items['tagi'] = tagi
            yield items



        next_page = 'https://www.goodreads.com/quotes?page='+ str(GoodreadsSpiderSpider.page_number)
        if GoodreadsSpiderSpider.page_number <= 100:
            GoodreadsSpiderSpider.page_number += 1
            yield response.follow(next_page, callback = self.parse)

# Zapisywanie scrapy crawl goodreads -o goodreads.csv