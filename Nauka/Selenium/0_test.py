from selenium import webdriver

driver_path = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=driver_path)


driver.get("https://www.nytimes.com")
headlines = driver.find_elements_by_class_name("balancedHeadline")
for headline in headlines:
    print(headline.text.strip())