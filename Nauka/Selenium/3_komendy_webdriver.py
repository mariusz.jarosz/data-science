from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

sciezka = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=sciezka)

driver.get('http://demo.automationtesting.in/Windows.html') # otwiera stronę

print(driver.title) # zwraca tytul strony
print(driver.current_url) # zwraca adres strony

driver.find_element_by_xpath('//*[@id="Tabbed"]/a/button').click() # naciśniecie przycisku

time.sleep(5) # odczekanie 5 sekund na następną akcje

# driver.close() # zamyka stronę

driver.quit() # zamknięcie wszystkich kart i przeglądarki



