from selenium import webdriver
from selenium.webdriver.common.by import By


sciezka = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'

driver = webdriver.Firefox(executable_path=sciezka)

driver.get('https://fs2.formsite.com/meherpavan/form2/index.html?1537702596407')

# ile jest pól do wprowadzenia

inputboxes = driver.find_elements(By.CLASS_NAME, 'text_field')

print(len(inputboxes)) # 8

# Jak wprowadzić dane

driver.find_element(By.ID,'RESULT_TextField-1').send_keys('Mariusz')
driver.find_element(By.ID, 'RESULT_TextField-2').send_keys('Jarosz')

driver.find_element_by_id('RESULT_TextField-3').send_keys('111111111')

# Jak sprawdzić status pola

status = driver.find_element(By.ID,'RESULT_TextField-1').is_displayed()
print(status)
