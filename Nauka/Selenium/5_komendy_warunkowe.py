from selenium import webdriver
from selenium.webdriver.common.keys import Keys



sciezka = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=sciezka)

driver.get('http://newtours.demoaut.com/')

ele = driver.find_element_by_name('userName')
print(ele.is_displayed()) # sprawdza czy element jest wyświetlony
print(ele.is_enabled())

pas = driver.find_element_by_name('password')
print(pas.is_displayed())
print(pas.is_enabled())

ele.send_keys('mercury')
pas.send_keys('mercury')

driver.find_element_by_name('login').click()

roundtrip_radio = driver.find_element_by_css_selector('input[value=roundtrip]')
print(roundtrip_radio.is_selected())

onetrip_radio = driver.find_element_by_css_selector('input[value=oneway]')
print(onetrip_radio.is_selected())

# driver.close()