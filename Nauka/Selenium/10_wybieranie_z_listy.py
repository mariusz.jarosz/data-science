from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time


sciezka = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path = sciezka)
driver.get('https://fs2.formsite.com/meherpavan/form2/index.html?1537702596407')


element = driver.find_element_by_id('RESULT_RadioButton-9')
drp = Select(element)

# # Wybór opcji poprzez widoczny tekst
# drp.select_by_visible_text('Evening')

# # Wybór opcji poprzez numer indeksu
# drp.select_by_index(2)

# # Wybór opcji poprzez wartość
# drp.select_by_value('Radio-0')


# Zliczenie wszystkich dostępnych opcji
print(len(drp.options))

# Pokazanie wszystkich dostępnych opcji

all_options = drp.options

for option in all_options:
    print(option.text)








# element = driver.find_element_by_xpath('//*[@for="RESULT_CheckBox-8_0"]')
# print(element.is_selected())
# element.click()

# element = driver.find_element_by_xpath('//*[@id="RESULT_CheckBox-8_0"]')
# print(element.is_selected())

