from selenium import webdriver
from selenium.webdriver.common.keys import Keys


sciezka = r'D:\Nauka_DS\Selenium\Cwiczenia\drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=sciezka)

driver.get('http://newtours.demoaut.com/')

# oczekiwanie na załadowanie strony np 10 sekund
driver.implicitly_wait(10)

assert 'Welcome: Mercury Tours' in driver.title # otwarcie strony trwa

driver.find_element_by_name('userName').send_keys('mercury')
driver.find_element_by_name('password').send_keys('mercury')

driver.find_elements_by_name('login').click()